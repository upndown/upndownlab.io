## Development Setup

Install Yarn. Use Yarn to install dependencies:

    $ yarn

### Development Server

    $ ./script/server

### Build

    $ make
