PARCEL := node_modules/.bin/parcel
SOURCE_FILES := $(shell find app -type f)

public/index.html: $(PARCEL) $(SOURCE_FILES)
	$(PARCEL) build --out-dir public app/index.html

$(PARCEL):
	yarn

.PHONY: clean
clean:
	rm -rf public

.PHONY: clobber
clobber: clean
	rm -rf node_modules
